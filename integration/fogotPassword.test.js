//WIP
describe('forgot-password', () => {

    // to override the global page
    let page;

    beforeAll(async() => {
        context = await browser.createIncognitoBrowserContext();
    });

    beforeEach(async () => {
        // Do stuff

        page = await context.newPage()
        try {
            await page.goto('http://localhost:8080/login', { waitUntil: "load" });
        }
        catch (err) {
            console.log('chrome is unexpectedly closed', err);
        }
    });

    afterEach(async() => {
       page.close();
    });

    it('wrong email without @', async () => {
        await page.waitFor('#login-form')
        await page.type('input[name="username"]', 'roman');
        await page.type('input[name="password"]', 'roman');
        await page.click('button[type=submit]');
        await page.screenshot({path: 'screenshot.png'});
        await page.waitFor('.error-message') 
        await expect(page).toMatch('Incorrect username or password.')
    });

    xit('wrong email without with non latin symbols', async () => {
    
        await page.type('#email', 'роман');
        await page.click('button[type=submit]');
        await page.waitFor(1000);
        await expect(page).toMatch('Please enter your email address. ')
    })

    xit('resets via reset button ', async () => {
        
        await page.type('#email', 'роман');
        await page.click('button[type=button]');
        const text = await page.$eval('#email', node => node.innerText);
        await expect(text).toMatch('');
        
    })
})