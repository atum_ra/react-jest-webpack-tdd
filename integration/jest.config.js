module.exports = {
    preset: 'jest-puppeteer',
    setupTestFrameworkScriptFile: './setupTestFrameworkScriptFile.js',
    // testRegex: '/integration/.*\\.test\\.js$',   
    testRegex: '/integration/.*.test.js$',   
    verbose: true,
    
      "globalSetup": "jest-environment-puppeteer/setup",
      "globalTeardown": "jest-environment-puppeteer/teardown",
      "testEnvironment": "jest-environment-puppeteer"
    
  }