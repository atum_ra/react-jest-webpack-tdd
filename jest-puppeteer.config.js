global.process.getuid = () => 0;

/*
- protocol: 'http' - that we actually wait until an http reply before server is considered to be running
- launchTimeout: 100000 - yeah, it can take over 30s to start a server. let's make it 1000
- usedPortAction: 'ignore'. Therefore if there is something responding on this port,
  let's assume you started server already there is no need to start it again
*/
module.exports = {
  launch: {
    headless: true,
  },
  server: {
    command: 'npm start',
    port: 8080,
    launchTimeout: 10000,
    protocol: 'http',
    usedPortAction: 'ignore'
  },
  browserContext: 'incognito'
}