# Template of react-redux-router-auth-saga-tdd app

## Install
To install dependencies

```shell
npm install
```

## Run
To run app

```shell
npm start
```

## Test with jest
It tests for components and saga flow

```shell
npm test
```

## Test with jest and puppeteer(WIP)
It tests user process flow with a whole app

```shell
npm run test:integration
```
