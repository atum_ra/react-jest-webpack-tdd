module.exports = {  
    "presets": ["@babel/preset-env", "@babel/preset-react"],
    "plugins": [
        "@babel/plugin-proposal-class-properties",
        /* By default this will be added to every file that requires it.
        This duplication is sometimes unnecessary, especially when your application 
        is spread out over multiple files. Here it comes in: all of the helpers will
        reference the module @babel/runtime to avoid duplication across your compiled
        output. The runtime will be compiled into your build. */
        "@babel/plugin-transform-runtime"
      ]
}