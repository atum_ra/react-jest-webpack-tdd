const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: { main: './src/index.js' },
    output: {
        path: path.join(__dirname, '/public'),
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: './public/',
        watchContentBase: true,
        publicPath: "/",
        historyApiFallback: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin({}),
        new MiniCssExtractPlugin({ filename: 'style.css' }),
    ],
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {loader: 'babel-loader'}
            }, {
            test: /\.(css|less)$/,
            use: [MiniCssExtractPlugin.loader, 'css-loader', 'less-loader']
            }
    ]},
    
};
