import { call, put, takeLatest } from 'redux-saga/effects';
import { loginService } from '../services/loginService';

export function* logInSaga({ payload }) {
  try {
    const { username, password } = payload;
    const response = yield call(loginService, username, password)
    yield put({ type: 'AUTHORIZATION_SUCCESS', username: response});
  } catch (error) {
    yield put({ type: 'AUTHORIZATION_FAIL', error: error });
  }
}

export default function* () {
  yield takeLatest('LOG_IN', logInSaga);

}
