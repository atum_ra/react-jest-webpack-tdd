import { AUTHORIZATION_SUCCESS, AUTHORIZATION_FAIL, SIGN_OUT } from './actionTypes';

const auth = (state = false, action) => {
  switch (action.type) {
    case AUTHORIZATION_SUCCESS:
      return { username: action.username };
    case SIGN_OUT:
    case AUTHORIZATION_FAIL:
      return { error: action.error };
    default:
      return state;
  }
};

export default auth;
