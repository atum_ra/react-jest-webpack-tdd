import React from 'react';

// Components //

const NotFound = () => (
  <React.Fragment>
    <h3>Page not found</h3>
  </React.Fragment>
);

export default NotFound;
