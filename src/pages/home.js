import React from 'react';

const Home = () => (
  <React.Fragment>
    <h3>Hello, this is tempate of template-react-redux-router-auth-saga-tdd app</h3>
    <ul>
      <li>

          <strong>Here you can find several technologies combined together:</strong>
          <br/>
          <ul>
            <li>React with TDD</li>
            <li>HOC for Auth and expandable routing</li>
            <li>Unit and component tests</li>
            <li>E2E tests</li>
          </ul>

      </li>
    </ul>
  </React.Fragment>
);

export default Home;
