import { call, put } from "redux-saga/effects";
import {loginService} from "../services/loginService";
import { logInSaga } from "../store/sagas";

const username = "admin";
const failedUsername = "f";
const password = "12345";

describe("signin flow", () => {

  test("signIn success", () => {
    const generator = logInSaga({ payload: { username, password }});
    expect(generator.next().value).toEqual(call(loginService, username, password));
    expect(generator.next().value).toEqual(put({ type: 'AUTHORIZATION_SUCCESS', username: undefined }));
  });

  test("signIn failure", () => {
    const generator = logInSaga( {payload: { "username" :failedUsername, password }});
    expect(generator.next().value).toEqual(call(loginService, failedUsername, password));
    expect(generator.throw().value).toEqual(put({ type: 'AUTHORIZATION_FAIL', error: undefined }));
  });
});
