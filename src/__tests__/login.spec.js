import React from 'react';
import ReactDOM from 'react-dom';
import  Enzyme,{shallow, mount } from 'enzyme';
import { Provider } from "react-redux"
import Adapter from 'enzyme-adapter-react-16';
import Login from '../pages/login';

import configureStore from 'redux-mock-store';

/* configure({ adapter: new Adapter() });
let driver;
beforeEach(() => (driver = appDriver())); */

Enzyme.configure({adapter: new Adapter()});

const initialState = {
  isAuthorized: null,
  error: ''
};

const mockStore = configureStore();
let store = mockStore(initialState)


it('Login page got name and pass and changed state', () => {

  const wrapper = mount(<Provider store={store}><Login /></Provider>);

  wrapper.find('[name="username"]').simulate('change', { target: { value: 'admin' }} );
  wrapper.find('[name="password"]').simulate('change', { target: { value: '12345' }} );
  wrapper.find('[type="submit"]').simulate('click');

  expect(JSON.stringify(wrapper.find(Login).childAt(0).state())).toEqual( '{\"username\":\"admin\",\"password\":\"12345\"}');
});


